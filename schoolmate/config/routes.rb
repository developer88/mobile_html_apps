Schoolmate::Application.routes.draw do
  root :to => 'diary#index'

  resources :users
  resources :sessions
  resources :notifications
  resources :profiles
  resources :finances
  resources :options do
    collection do
      get  :index
      get  :about
      get  :feedback
      post :send_feedback
    end
  end
  resources :years do
    resources :terms do
      member do 
        get 'assign_subject'
        post 'add_subject'
      end
    end
  end
  resources :exams
  resources :exam_watchers
  resources :subjects
  resources :marks

  match "login"     => "sessions#new", :via => :get
  match "logout"    => "sessions#destroy", :via => :get
  match "signup"    => "users#new", :via => :get
  match 'diary'     => 'diary#index'
  match 'diary_add' => 'diary#add'
  match 'home'      => 'home#index', :via => :get
  match 'study'     => 'study#index', :via => :get
  



  

end
