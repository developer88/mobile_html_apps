class CreateYearsTable < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.integer :user_id
      t.integer :code

      t.timestamps
    end
  end
end
