class CreateFeedbacksTable < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.integer :user_id
      t.text    :message
      t.string  :rate

      t.timestamps
    end
  end
end
