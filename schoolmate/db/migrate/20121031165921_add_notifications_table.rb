class AddNotificationsTable < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.boolean :read, :default => false
      t.boolean :action
      t.string  :data
      t.string  :object

      t.timestamps
    end
  end
end
