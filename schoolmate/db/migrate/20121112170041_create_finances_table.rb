class CreateFinancesTable < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :user_id
      t.float   :value

      t.timestamps
    end
  end
end
