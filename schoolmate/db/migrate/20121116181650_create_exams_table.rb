class CreateExamsTable < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.integer  :user_id
      t.integer  :year_id
      t.string   :type
      t.string   :name
      t.datetime :perform_date
      t.integer  :subject_id

      t.timestamps
    end
  end
end
