class CreateSubjectsTermsTable < ActiveRecord::Migration
  def change
    create_table :subjects_terms do |t|
      t.integer :subject_id
      t.integer :term_id
    end
  end
end
