class CreateMarksTable < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.integer   :subject_id
      t.integer   :mark
      
      t.timestamps
    end
  end
end
