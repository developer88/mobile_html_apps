class CreateExamWatchersTable < ActiveRecord::Migration
  def change
    create_table :exam_watchers do |t|
      t.integer  :user_id
      t.integer  :subject_id
      t.integer  :mark
      t.datetime :deadline_date

      t.timestamps
    end
  end
end
