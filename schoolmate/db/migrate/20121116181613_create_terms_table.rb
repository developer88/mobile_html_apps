class CreateTermsTable < ActiveRecord::Migration
  def change
    create_table :terms do |t|
      t.integer  :user_id
      t.integer  :year_id
      t.string   :name
      t.date     :start_date
      t.date     :finish_date

      t.timestamps
    end
  end
end
