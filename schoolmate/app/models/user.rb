class User < ActiveRecord::Base
	attr_accessible :email, :password, :password_confirmation
    has_secure_password
	validates_presence_of :password, :on => :create

	belongs_to :profile
    has_many   :notifications
    has_many   :feedbacks
    has_many   :finances
    has_many   :subjects
    has_many   :years
    has_many   :terms

end
