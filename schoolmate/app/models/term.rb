class Term < ActiveRecord::Base

    belongs_to :user
    belongs_to :year
    has_and_belongs_to_many :subjects

    attr_accessible :year_id, :user_id, :name, :start_date, :finish_date
    validates :name, :presence => true
    validates :start_date, :presence => true
    validates :finish_date, :presence => true

    def assign_subjects(subjects_arr)
        self.subjects.clear
        arr = subjects_arr.keys.map{|k| k.to_i } 
        self.subjects << Subject.find(arr)
        save
    end

end