class Year < ActiveRecord::Base

    belongs_to :user
    has_many :terms

    attr_accessible :code, :user_id
    validates :code, :presence => true
    validates :code, :numericality => { :only_integer => true }

end