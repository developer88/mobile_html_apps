class Subject < ActiveRecord::Base

    belongs_to :user
    has_and_belongs_to_many :terms

    attr_accessible :name, :user_id
    validates :name, :presence => true

end