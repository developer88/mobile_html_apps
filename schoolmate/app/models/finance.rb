class Finance < ActiveRecord::Base
    attr_accessible :value, :user_id
    validates :value, :presence => true

    belongs_to :user

end