module FinancesHelper

    def date_with_month(finance)
        localize finance.created_at, :format => '%B %d, %Y'
    end    

end
