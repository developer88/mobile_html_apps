module StudyHelper

    def finish_date_for_term
        time = Time.now
        term = 6
        year = time.month + term > 12 ? time.year + 1 : time.year
        month = time.month + term > 12 ? time.month + term - 12 : time.month + term
        Time.utc(year, month).strftime("%d-%m-%Y")
    end

end