module ApplicationHelper

	def display_messages
		html = ""
		[:info, :error, :alert].each do |key|
			html << "<div><span>#{flash[key]}</span></div>" if flash[key]
		end
		html = "<div class='ui-bar ui-bar-e'>" << html << "</div>" if html.length > 0
		html
	end

    def there_are_notifications?
        #current_user && controller.controller_name != "notifications" && current_user.notifications.size
        false
    end

    def proper_date(date)
        return "" unless date
        date.strftime("%d-%m-%Y")
    end

end
