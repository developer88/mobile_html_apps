class FinancesController < ApplicationController
  before_filter :require_authentication
  layout "mobile"
  
  def index
    @finances = current_user.finances    
  end

  def edit
    @finance = Finance.find(params[:id])
  end

  def destroy
    finance = Finance.find(params[:id])
    if finance.user_id != current_user.id
        flash[:error] = t('finance.not_belong')
        redirect_to finances_path
    end
    if finance.delete
        flash[:info] = t('finance.deleted')
    else
        flash[:error] = t('finance.delete_error')
    end
    redirect_to finances_path 
  end

  def create
    finance = Finance.new(value:params[:value], user_id:current_user.id)
    if finance.save
      flash[:info] = t('finance.created')
      redirect_to finances_path  
    else
      flash[:error] = t('finance.create_error')
      redirect_to new_finance_path
    end    
  end

  def new
    @finance = Finance.new
  end

end
