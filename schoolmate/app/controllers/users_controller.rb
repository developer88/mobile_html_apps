class UsersController < ApplicationController
  layout "mobile"
  
  def new
    @user = User.new
    clear_messages
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      redirect_to root_url, :notice => "Signed up!"
    else
      flash.alert = @user.errors.full_messages.join(", ")
      render "new"
    end
  end


end
