# encoding: utf-8
class OptionsController < ApplicationController
    before_filter :require_authentication
    layout "mobile"
	
	def index
		@profile = Profile.where(:user_id => current_user.id).first_or_create
	end

    def feedback
        @feedback = Feedback.new
    end

    def send_feedback
        @feedback = Feedback.new(message:params[:message])
        if @feedback.save
            flash[:info] = t('feedback.sent')
            redirect_to options_path
        else
            flash[:error] = t('feedback.error')
            redirect_to feedback_options_path
        end        
    end

    def about

    end
end
