# encoding: utf-8
class SessionsController < ApplicationController
  layout "mobile"
  
  def new
    clear_messages
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, :notice => t('session.logged_in')
    else
      flash.alert = t('session.wrong_credentials')
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => t('session.logged_out')
  end


end
