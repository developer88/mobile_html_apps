class YearsController < ApplicationController
  before_filter :require_authentication
  layout "mobile"
  
  def edit
    @year = Year.find(params[:id])
  end

  def destroy
    year = Year.find(params[:id])
    if year.user_id != current_user.id
        flash[:error] = t('year.not_belong')
        redirect_to study_path
    end
    if year.delete
        flash[:info] = t('year.deleted')
    else
        flash[:error] = t('year.delete_error')
    end
    redirect_to study_path 
  end

  def create
    year = Year.new(code:params[:code], user_id:current_user.id)
    if year.save
      flash[:info] = t('year.created')
      redirect_to study_path  
    else
      flash[:error] = t('year.create_error')
      redirect_to new_year_path
    end    
  end

  def new
    @year = Year.new
  end

  def update
    year = Year.find(params[:id])
    if year.user_id != current_user.id
        flash[:error] = t('year.not_belong')
        redirect_to study_path
    end
    year.code = params[:code]
    if year.save
        flash[:info] = t('year.updated')
    else
        flash[:error] = t('year.update_error')
    end    
    redirect_to study_path 
  end

end
