class SubjectsController < ApplicationController
  before_filter :require_authentication
  layout "mobile"
  
  def index
    @subjects = current_user.subjects    
  end

  def edit
    @subject = Subject.find(params[:id])
  end

  def destroy
    subject = Subject.find(params[:id])
    if subject.user_id != current_user.id
        flash[:error] = t('subject.not_belong')
        redirect_to subjects_path
    end
    if subject.delete
        flash[:info] = t('subject.deleted')
    else
        flash[:error] = t('subject.delete_error')
    end
    redirect_to subjects_path 
  end

  def create
    subject = Subject.new(name:params[:name], user_id:current_user.id)
    if subject.save
      flash[:info] = t('subject.created')
      redirect_to subjects_path  
    else
      flash[:error] = t('subject.create_error')
      redirect_to new_subject_path
    end    
  end

  def new
    @subject = Subject.new
  end

  def update
    subject = Subject.find(params[:id])
    if subject.user_id != current_user.id
        flash[:error] = t('subject.not_belong')
        redirect_to subjects_path
    end
    subject.name = params[:name]
    if subject.save
        flash[:info] = t('subject.updated')
    else
        flash[:error] = t('subject.update_error')
    end    
    redirect_to subjects_path 
  end

end
