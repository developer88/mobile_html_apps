# encoding: utf-8
class ProfilesController < ApplicationController
    before_filter :require_authentication
    layout "mobile"

    def update
        @profile = Profile.find(params[:id])
        @profile.name = params[:name]
        @profile.children_count = params[:children_count]
        if @profile.save
            flash[:info] = t('profile.saved')
        else
            flash[:error] = t('profile.error')
        end
        respond_to do |format|
            format.js { render :nothing => true}
            format.json { render :nothing => true}
            format.html { redirect_to options_path }
        end
    end
    
end
