class TermsController < ApplicationController
  before_filter :require_authentication
  layout "mobile"
  
  def edit
    @term = Term.find(params[:id])
    @year = Year.find(params[:year_id])
  end

  def destroy
    term = Term.find(params[:id])
    if term.user_id != current_user.id
        flash[:error] = t('term.not_belong')
        redirect_to study_path
    end
    if term.delete
        flash[:info] = t('term.deleted')
    else
        flash[:error] = t('term.delete_error')
    end
    redirect_to study_path 
  end

  def create
    term = Term.new(
      name:params[:name], user_id:current_user.id, 
      year_id:params[:year_id],
      start_date:params[:start_date],
      finish_date:params[:finish_date]
      )
    year = Year.find(params[:year_id])
    if term.save
      flash[:info] = t('term.created')
      redirect_to study_path  
    else
      flash[:error] = t('term.create_error')
      redirect_to new_year_term_path(year)
    end    
  end

  def new
    @year = Year.find(params[:year_id])
    @term = Term.new
  end

  def update
    term = Term.find(params[:id])    
    if term.user_id != current_user.id
        flash[:error] = t('term.not_belong')
        redirect_to study_path
    end
    term.name = params[:name]
    term.start_date = params[:start_date]
    term.finish_date = params[:finish_date]
    if term.save
        flash[:info] = t('term.updated')
    else
        flash[:error] = t('term.update_error')
    end    
    redirect_to study_path 
  end

  def assign_subject
    @term = Term.find(params[:id])
    @year = Year.find(params[:year_id])
    @subjects = current_user.subjects.includes(:terms => :subjects)
    #raise current_user.subjects.includes(:terms => :subjects).explain
    #@term_subjects = @term.subjects
  end

  def add_subject
    term = Term.find(params[:id])
    year = Year.find(params[:year_id])
    if term.assign_subjects(params[:subjects])
      flash[:info] = t('term.subjects_assigned')
      redirect_to study_path
    else
      flash[:error] = t('term.subjects_assign_error')
      redirect_to assign_subject_year_term_path(year, term)
    end
    
  end

end
