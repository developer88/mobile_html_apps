class ApplicationController < ActionController::Base
  protect_from_forgery
  force_ssl

  http_basic_authenticate_with name: "admin", password: "qwerty", if: lambda { %w[stage production].include? Rails.env }

  before_filter :prepare_for_mobile
  before_filter :set_locale
  before_filter :denie_desktop_access

  helper_method :current_user, :authenticated? 
  helper_method :mobile_device?  
 
  def set_locale
    #I18n.locale = params[:locale] || I18n.default_locale
    logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
    I18n.locale = extract_locale_from_accept_language_header
    logger.debug "* Locale set to '#{I18n.locale}'"    
  end

  private

  def denie_desktop_access
    return true unless Rails.env.production?
    redirect_to home_path  if !mobile_device? && controller_name.to_s != "home"
    redirect_to diary_path if  mobile_device? && controller_name.to_s == "home"
  end

  def mobile_device?
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      request.user_agent =~ /Mobile|webOS|Android/
    end
  end  

  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile]
    #request.format = :mobile if mobile_device?
  end  

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def authenticated?
    session[:user_id].present? 
  end

  def require_authentication
    redirect_to login_path unless authenticated?
  end

  def clear_messages
    flash.alert = nil
  end

  def extract_locale_from_accept_language_header
    #request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    "ru" # use russian by default 
  end  

end
