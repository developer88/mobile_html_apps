thecalcApp = angular.module 'thecalc', ['ionic', 'thecalc.services', 'thecalc.controllers', 'ui.graph', 'ui.chart', 'pickadate']

thecalcApp.config ($stateProvider, $urlRouterProvider)->

  $stateProvider

    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    .state('tab.pet-index', {
      url: '/pets',
      views: {
        'pets-tab': {
          templateUrl: 'templates/pet-index.html',
          controller: 'PetIndexCtrl'
        }
      }
    })

    .state('tab.pet-detail', {
      url: '/pet/:petId',
      views: {
        'pets-tab': {
          templateUrl: 'templates/pet-detail.html',
          controller: 'PetDetailCtrl'
        }
      }
    })

    .state('tab.adopt', {
      url: '/adopt',
      views: {
        'adopt-tab': {
          templateUrl: 'templates/adopt.html'
        }
      }
    })

    .state('tab.about', {
      url: '/about',
      views: {
        'about-tab': {
          templateUrl: 'templates/about.html'
        }
      }
    })

    # new ###################
    .state('tab.stat', {
      url: '/stat',
      views: {
        'stat-tab': {
          templateUrl: 'templates/stat.html',
          controller: 'StatCtrl'
        }
      }
    })

    .state('tab.user', {
      url: '/user',
      views: {
        'user-tab': {
          templateUrl: 'templates/user.html',
          controller: 'UserCtrl'
        }
      }
    })

    .state('tab.list', {
      url: '/list',
      views: {
        'list-tab': {
          templateUrl: 'templates/list.html',
          controller: 'ListCtrl'
        }
      }
    })

    .state('tab.calendar', {
      url: '/calendar',
      views: {
        'calendar-tab': {
          templateUrl: 'templates/calendar.html',
          controller: 'CalendarCtrl'
        }
      }
    })  

    .state('tab.catalog', {
      url: '/catalog',
      views: {
        'catalog-tab': {
          templateUrl: 'templates/catalog.html',
          controller: 'CatalogCtrl'
        }
      }
    }) 

    .state('tab.calendar_item', {
      url: '/calendar/:dateStr',
      views: {
        'calendar-tab': {
          templateUrl: 'templates/calendar.html',
          controller: 'CalendarCtrl'
        }
      }
    })      

    .state('tab.item', {
      url: '/item/:itemId',
      views: {
        'catalog-tab': {
          templateUrl: 'templates/item.html',
          controller: 'CatalogItemCtrl'
        }
      }
    })           

  $urlRouterProvider.otherwise '/tab/stat'

