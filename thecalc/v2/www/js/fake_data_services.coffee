
####
# Fake user Statistic
####
thecalcServices.factory 'UserFakeStatDataService', ->

  calculateLimit = ->
    return 500

  # public
  service = {}

  service.list = ->
    []

  service.limit = ->
    return calculateLimit()

  service.current = ->
    return 123

  service.get = (type_of_period, length) ->
    if type_of_period == "week"
      return [[['23-May-08', 578.55], ['20-Jun-08', 566.5], ['25-Jul-08', 480.88]]]
    if type_of_period == "month"
      if length > 1
        return [[['23-May-08', 578.55], ['20-Jun-08', 566.5], ['25-Jul-08', 480.88], ['22-Aug-08', 509.84],
            ['26-Sep-08', 454.13], ['24-Oct-08', 379.75], ['21-Nov-08', 303], ['26-Dec-08', 308.56],
            ['23-Jan-09', 299.14], ['20-Feb-09', 346.51], ['20-Mar-09', 325.99], ['24-Apr-09', 386.15]]]
      if length == 1
        return [[['20-Feb-09', 346.51], ['20-Mar-09', 325.99], ['24-Apr-09', 386.15]]]
    
    return []

  service.getByDate = (date_str)->
    return []


  service.getFilledDates = (type_of_period, length) ->
    return ["2014-04-08", "2014-04-15"] 
    

  return service


####
# Fake data for development use
####

thecalcServices.factory 'UserFakeDataService', (UserFakeStatDataService) ->

  service = {}

  service.get = ->
    console.log 'User get'
    return { name: 'Andrey Eremin', weight: 75, growth: 183, age: 25, image: "", aim_id: "basic1", stat: UserFakeStatDataService }

  service.save = (new_value)->
  	console.log 'User saved'
  	return true

  return service

