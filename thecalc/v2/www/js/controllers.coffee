
# old
thecalcControllers.controller 'PetIndexCtrl', ($scope, PetService)->
  $scope.pets = PetService.all()

thecalcControllers.controller 'PetDetailCtrl', ($scope, $stateParams, PetService)->
  $scope.pet = PetService.get($stateParams.petId)

# new

####
# User Statistic Controller
####
thecalcControllers.controller 'StatCtrl', ($scope, $stateParams, UserService, $state)->
  # variables
  $scope.user = UserService.get()
  $scope.graph_data = []
  $scope.view_states = {
    graph_period: "week"
  }  
  $scope.graphOptions = {
    axesDefaults: {
      show: false,
      tickOptions: {
        showMark: true,
        showGridline: false,
      },
      showTicks: true,
      showTickMarks: false
    },
    highlighter: {
      show: true,
      sizeAdjust: 7.5
    },
    cursor: {
      show: false
    },
    axes:{
      xaxis:{
        renderer:$.jqplot.DateAxisRenderer,
        tickOptions:{
          formatString:'%b&nbsp;%#d'
        } 
      },
      yaxis:{
        tickOptions:{
          formatString:'%.2f ккал'
          }
      }
    }    
  }  

  # watchers
  $scope.$watchCollection 'view_states.graph_period', (newValue)->
    $scope.drawGraph newValue


  # public methods ?????? TODO
  $scope.rightButton = ->
  	alert('asdasd')

  $scope.drawGraph = (period) ->
  	$scope.graph_data = []
  	switch period
  	  when "week" then $scope.graph_data = $scope.user.stat.get('week', 1)
  	  when "month" then $scope.graph_data = $scope.user.stat.get('month', 1)
  	  when "3months" then $scope.graph_data = $scope.user.stat.get('month', 3)
    return true

  $scope.goToDate = (ev, seriesIndex, pointIndex, data)->
    # if stat data would sort by date than we can use pointIndex
    date = $scope.graph_data[0][pointIndex]
    console.log($scope.graph_data)
    console.log(date)
    console.log(ev)
    console.log seriesIndex
    console.log pointIndex
    console.log data
    console.log 'Value: ' + date[0]
    console.log '/calendar/'+date[0]
    $state.go('tab.calendar_item', {dateStr: data[0]});
    #$location.path( '#/calendar/'+date[0] )
    
    #date =  new Date(ev.timeStamp*1000)
    #months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    #console.log "Date: " + date.getDay().toString() + "-" + months[date.getMonth()].toString() + "-" + date.getFullYear().toString()
    #alert('Click')
    return true

  return

####
# User Controller
####
thecalcControllers.controller 'UserCtrl', ($scope, $stateParams, UserService, AimService)->
  # variables
  $scope.user = UserService.get()
  $scope.aims = AimService.all()

  # watchers
  $scope.$watchCollection 'user', (newValue, oldValue)->
    UserService.save($scope.user)
  
  # public methods


  return

####
# List Controller
####
thecalcControllers.controller 'ListCtrl', ($scope, $stateParams, UserService, AimService)->
  # variables
  $scope.user = UserService.get()
  $scope.aims = AimService.all()
  
  # public methods


  return

####
# Calendar Controller
####
thecalcControllers.controller 'CalendarCtrl', ($scope, $stateParams, UserService)->
  # variables
  $scope.user = UserService.get()
  $scope.calendarData = $scope.user.stat.getFilledDates('week', 1)
  $scope.selectedDate = {name: "", stat: []}
  
  # public methods

  # helpers
  $scope.dateIsSelected = (date)->
    return false if date.length == 0
    $scope.selectedDate.name = date
    $scope.selectedDate.stat = $scope.user.stat.getByDate(date)

    console.log "smth"
    console.log date


  # initialization
  console.log $stateParams.dateStr
  # TODO convert from unix
  $scope.dateIsSelected($stateParams.dateStr) if $stateParams.dateStr

  return

####
# Catalog Controller
####
thecalcControllers.controller 'CatalogCtrl', ($scope, $stateParams, UserService, MealService)->
  
  # variables
  #$scope.user = UserService.get()
  $scope.items = MealService.all()
  
  # public methods
  $scope.AddToMenu = (id)->
    alert('Added')


  return


####
# Catalog's Item Controller
####
thecalcControllers.controller 'CatalogItemCtrl', ($scope, $stateParams, UserService, MealService)->
  
  # variables
  #$scope.user = UserService.get()
  $scope.item = MealService.get($stateParams.itemId)
  console.log "dasdasd"
  
  # public methods


  return