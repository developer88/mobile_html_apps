####
# Graph derective
####

angular.module('ui.graph', []).directive 'uiGraph', ->
  return  {
    restrict: 'EACM',
    template: '<div></div>',
    replace: true,
    link: ($scope, element, attrs)->  
      renderGraph = ->
        data = $scope.$eval(attrs.uiGraph);
        options = $scope.$eval(attrs.graphOptions)
        id = $(element).attr('id')
        click_callback = $scope.$eval(attrs.graphClick)

        if !angular.isObject(options)
          throw 'Invalid ui.graph options attribute'

        if angular.isUndefined(id)
          throw 'Invalid ui.graph id attribute'

        if angular.isArray(data)   
          $(element).unbind "jqplotDataClick"
          jQuery.jqplot(id, data, options).destroy()
          $(element).html ''
          jQuery.jqplot id, data, options
          if angular.isFunction(click_callback)
            $(element).bind 'jqplotDataClick', (ev, seriesIndex, pointIndex, data)->
              click_callback ev, seriesIndex, pointIndex, data 

      $scope.$watch attrs.uiGraph, (value)->
        renderGraph()

      $scope.$watch attrs.graphOptions, (value)->
        renderGraph()        

  }