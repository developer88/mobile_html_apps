

# Old
thecalcServices.factory 'PetService', ->
  pets = [
    { id: 0, title: 'Cats', description: 'Furry little creatures. Obsessed with plotting assassination, but never following through on it.' },
    { id: 1, title: 'Dogs', description: 'Lovable. Loyal almost to a fault. Smarter than they let on.' },
    { id: 2, title: 'Turtles', description: 'Everyone likes turtles.' },
    { id: 3, title: 'Sharks', description: 'An advanced pet. Needs millions of gallons of salt water. Will happily eat you.' }
  ]

  
  # define public methods
  service = {}
  

  service.all = ->
    pets

  service.get = (petId) ->
    pets[petId]

  return service






# New

####
# Settings
#
# To store settings for application
####
thecalcServices.factory 'Settings', ->

  is_local = ->
    return (typeof CORDOVA_JS_BUILD_LABEL == 'undefined')

  # public
  settings = {}

  settings.local = is_local()

  return settings

####
# Aim
####
thecalcServices.factory 'AimService', ->

  aims = [
    {name: 'Basic', id: 'basic1', enabled: true},
    {name: 'Basic Complex', id: 'basic_complex', enabled: true}
  ]

  # public
  service = {}

  service.all = ->
    return aims

  return service

####
# User
# Some kind of abstraction that provide base methods for User - save and get
####
thecalcServices.factory 'UserService', (UserFakeDataService, Settings, UserCordovaDataService) ->

  object = ->
    console.log Settings.local
    return UserFakeDataService if Settings.local == true
    return UserCordovaDataService if Settings.local == false

  # public
  service = {}

  service.get = ->
    return object().get()

  service.save = (new_user_obj)->
    return object().save(new_user_obj)

  return service

####
# Catalog of Meals
####
thecalcServices.factory 'MealService', () ->

  # public
  service = {}
  temp_data = [{id: 1, name: "Макароны по Флотски", image: "", kkal: 100, description: "Вкусные макароны", energy: "100 чего-то там"}, 
    {id: 2, name: "Картошка", image: "", kkal: 341, description: "Ням", energy: "1009 чего-то там"},
    {id: 3, name: "Баклажаны", image: "", kkal: 540, description: "Большой такой длиный текст для проверки того как он будет отображаться и должно отображаться очень даже хорошо", energy: "1000 чего-то там"},
    {id: 4, name: "Курица с яблоком", image: "", kkal: 150, description: "Вкусные макароны1", energy: "1001 чего-то там"},
    {id: 5, name: "Пельмени", image: "", kkal: 230, description: "Проверка 2", energy: "1002 чего-то там"},
    {id: 6, name: "Гамбургер", image: "", kkal: 1000, description: "Макдональдс (с)", energy: "1003 чего-то там"}]

  service.all = ->
    return temp_data

  service.get = (id)->
    return meal for meal in service.all() when meal.id == parseInt(id)

  return service  

####
# User Statistic
####
thecalcServices.factory 'UserStatService', ->

  calculateLimit = ->
    return 500

  # public
  service = {}

  service.list = ->
    []

  service.limit = ->
    return calculateLimit()

  service.current = ->
    return 123

  service.get = (type_of_period, length) ->
    if type_of_period == "week"
      return [[['23-May-08', 578.55], ['20-Jun-08', 566.5], ['25-Jul-08', 480.88]]]
    if type_of_period == "month"
      if length > 1
        return [[['23-May-08', 578.55], ['20-Jun-08', 566.5], ['25-Jul-08', 480.88], ['22-Aug-08', 509.84],
            ['26-Sep-08', 454.13], ['24-Oct-08', 379.75], ['21-Nov-08', 303], ['26-Dec-08', 308.56],
            ['23-Jan-09', 299.14], ['20-Feb-09', 346.51], ['20-Mar-09', 325.99], ['24-Apr-09', 386.15]]]
      if length == 1
        return [[['20-Feb-09', 346.51], ['20-Mar-09', 325.99], ['24-Apr-09', 386.15]]]
    
    return []

  service.getByDate = (date_str)->
    return []
    

  return service






  
        