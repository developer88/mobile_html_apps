angular.module('saveapp.services', [])

# Settings for 
.factory 'Settings', ->

  # If in development mode
  is_local = ->
    return (typeof CORDOVA_JS_BUILD_LABEL == 'undefined')

  # public
  settings = {}

  settings.local = is_local()

  return settings

# Credit and Debit categories
.factory 'Categories', (Spendings, Settings)->

  spendings = Spendings
  settings = Settings

  categories = [
    { id: 10, name: 'Cash', type: 'debit'},
    { id: 1, name: 'Debit Card', type: 'debit'},
    { id: 2, name: 'Transport', type: 'credit'},
    { id: 3, name: 'Internet services', type: 'credit'}
  ]

  object = {}

  object.all = ->
    if settings.local == true
      return categories
    else
      throw "Finish it1"
  object.get = (catId)-> 
    item = $.grep object.all(), (obj)->
      obj.id == parseInt(catId)
    return item[0]    
  object.last_id = ->
    object.all()[object.all().length].id        
  object.credit = ->
    $.grep object.all(), (obj)->
      obj.type == 'credit'
  object.debit = ->
    $.grep object.all(), (obj)->
      obj.type == 'debit'  
  object.credit_ids = ->
    $.map credit(), ( obj, i )->
      obj.id
  object.debit_ids = ->
    $.map debit(), ( obj, i )->
      obj.id      
  object.create = (name, type)->
    if settings.local == true
      new_object = {id: object.last_id(), name: name, type: type}
      categories.push(new_object)
      return new_object
    else
      throw "Finish it2"
  object.update = (id, name)->
    obj = object.get(id)
    return false unless obj
    if settings.local == true
      obj.name = name
      return true
    else
      throw "Finish it3"  
  object.delete = (id)->
    console.log(spendings.by_cat(id).length)
    return false if spendings.by_cat(id).length > 0
    if settings.local == true
      indexes = $.map object.all(), (obj, index)->
        return index if obj.id == id
      console.log(indexes)
      return false unless indexes[0] > -1
      object.all().splice(indexes[0], 1);
      return true        
    else
      throw "Finish it4"    
  object.can_be_deleted = (id)->
    return spendings.by_cat(id).length == 0

  return object

# Stores all user's spendings
.factory 'Spendings', (Settings)->

  settings = Settings

  spendings = [
    {id: 0, value: 1.43, from: 0, to: 3, date: '2014-03-01'},
    {id: 1, value: 5, from: 1, to: 2, date: '2014-02-05'},
    {id: 2, value: 100, from: null, to: 1, date: '2014-01-01'}
  ]

  object = {}

  object.all = ->
    if settings.local == true
      return spendings
    else
      throw "Finish it5"
  object.get = (statId)->
    console.log(object.all())
    console.log(statId)
    item = $.grep object.all(), (obj)->
      obj.id == parseInt(statId)
    return item[0]
  object.last_id = ->
    return object.all()[object.all().length].id
  object.by_cat = (cat_id)->
    return $.grep object.all(), (obj)->
      (obj.to == cat_id || obj.from == cat_id)
  object.create = (to_id, from_id, value, date)->
    if settings.local == true
      new_object = {id: object.last_id(), value: value, from: from_id, to: to_id, date: date}
      spendings.push(new_object)
      return new_object
    else
      throw "Finish it6"
  object.update = (id, to_id, from_id, value, date)->
    obj = object.get(id)
    return false unless obj
    if settings.local == true
      obj.to = to_id
      obj.from = from_id
      obj.value = value
      obj.date = date
      return true
    else
      throw "Finish it7"
  object.delete = (id)->
    if settings.local == true
      indexes = $.map object.all(), (obj, index)->
        return index if obj.id == id
      return false unless indexes[0] > -1
      object.all().splice(indexes[0], 1);
      return true
    else
      throw "Finish it8"     
  
  return object

# Methods to work with user's spending
.factory 'SpendingsStatHelper', (Spendings, Categories)->

  spendings = Spendings
  categories = Categories

  object = {}

  object.debit = ->
    $.grep spendings.all(), (obj)->
      (obj.from == null && $.inArray(obj.to, categories.debit_ids))
  object.credit = ->
    $.grep spendings.all(), (obj)->
      (obj.from == null && $.inArray(obj.to, categories.credit_ids)) 
  object.date = (date, type, way)->
    objs = object.debit() if type == 'debit' 
    objs = object.credit() if type != 'debit' 
    $.grep objs, (obj)->
      (way == 'till' && Date.new(obj.date).getDate() < Date.new(date).getDate()) || 
      (way == 'exact' && Date.new(obj.date).getDate() == Date.new(date).getDate()) || 
      (way == 'since' && Date.new(obj.date).getDate() > Date.new(date).getDate()) 
  object.till_date = (date, type)->
    object.date(date, type, 'till')
  object.by_date = (date, type)->
    object.date(date, type, 'exact')
  object.since_date = (date, type)->
    object.date(date, type, 'since') 
  object.spendings = ->
    spendings.all()
  object.sort = (data, order, field)->
    data.sort (a, b)->
      field1 = a[field]
      field2 = b[field]
      if field == 'date'
        field1 = new Date(field1)
        field2 = new Date(field2)
      if field == 'value'
        field1 = parseInt(field1)
        field2 = parseInt(field2)
      return 0 if field1 == field2
      return 1 if field1 > field2 && order == 'asc'
      return 1 if field2 > field1 && order == 'desc'
      return -1 
  object.sort_by_date = (data, order)->
    order = 'asc' if ['asc', 'desc'].indexOf(order) == -1
    object.sort(data, order, 'date')
  object.sort_by_value = (data, order)->
    order = 'asc' if ['asc', 'desc'].indexOf(order) == -1
    object.sort(data, order, 'value')  

  object.spendings = spendings
  object.categories = categories    

  return object

# Stores user's data
.factory 'Users', ->

  object = {}

  return object










.factory 'Friends', ->
  # Might use a resource here that returns a JSON array

  # Some fake testing data
  friends = [
    { id: 0, name: 'Scruff McGruff' },
    { id: 1, name: 'G.I. Joe' },
    { id: 2, name: 'Miss Frizzle' },
    { id: 3, name: 'Ash Ketchum' }
  ]

  return {
    all: ->
      return friends
    get: (friendId)->
      # Simple index lookup
      return friends[friendId]
  }

