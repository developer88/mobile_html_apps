# angular.module is a global place for creating, registering and retrieving Angular modules
# 'saveapp' is the name of this angular module example (also set in a <body> attribute in index.html)
# the 2nd parameter is an array of 'requires'
# 'saveapp.services' is found in services.js
# 'saveapp.controllers' is found in controllers.js
angular.module('saveapp', ['ionic', 'saveapp.controllers', 'saveapp.services'])

.run ($ionicPlatform)->
  $ionicPlatform.ready ->
    # Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    # for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) 
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)

    if(window.StatusBar) 
      # org.apache.cordova.statusbar required
      StatusBar.styleDefault()
    
.config ($stateProvider, $urlRouterProvider)->

  # Ionic uses AngularUI Router which uses the concept of states
  # Learn more here: https://github.com/angular-ui/ui-router
  # Set up the various states which the app can be in.
  # Each state's controller can be found in controllers.js

  $stateProvider

    # setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    # Each tab has its own nav history stack:

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })
    
    # Spendings 

    .state('tab.spendings', {
      url: '/spendings',
      views: {
        'tab-spendings': {
          templateUrl: 'templates/tab-spendings.html',
          controller: 'SpendingsCtrl'
        }
      }
    })
    .state('tab.spending', {
      url: '/spendings/:spendingId',
      views: {
        'tab-spendings': {
          templateUrl: 'templates/tab-spending.html',
          controller: 'SpendingCtrl'
        }
      }
    })

    # Categories

    .state('tab.categories', {
      url: '/categories',
      views: {
        'tab-categories': {
          templateUrl: 'templates/tab-categories.html',
          controller: 'CategoriesCtrl'
        }
      }
    })
    .state('tab.category', {
      url: '/categories/:categoryId',
      views: {
        'tab-categories': {
          templateUrl: 'templates/tab-category.html',
          controller: 'CategoryCtrl'
        }
      }
    })









    #.state('tab.friends', {
    #  url: '/friends',
    #  views: {
    #    'tab-friends': {
    #      templateUrl: 'templates/tab-friends.html',
    #      controller: 'FriendsCtrl'
    #    }
    #  }
    #})
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    })

  # if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash')

