angular.module('saveapp.controllers', [])

.controller 'DashCtrl', ($scope) ->
  ctrl = {}
  return ctrl

# List of all spendings
.controller 'SpendingsCtrl', ($scope, SpendingsStatHelper) ->
  ctrl = {}

  $scope.spendings_helper = SpendingsStatHelper
  $scope.spendings_data = $scope.spendings_helper.sort_by_date($scope.spendings_helper.spendings.all(), 'desc')
  
  console.log($scope.spendings_data)

  return ctrl

# Information about spending
.controller 'SpendingCtrl', ($scope, $stateParams, SpendingsStatHelper)->

  ctrl = {}
  $scope.spendings_helper = SpendingsStatHelper
  $scope.spending = $scope.spendings_helper.spendings.get($stateParams.spendingId)
  console.log($stateParams.spendingId)
  console.log($scope.spending)
  console.log($scope.spendings_helper)

  return ctrl

.controller 'AccountCtrl', ($scope)->
  ctrl = {}
  return ctrl

# List of all categories
.controller 'CategoriesCtrl', ($scope, Categories) ->
  ctrl = {}

  #$scope.categories_data = Categories.all();
  $scope.categories = Categories
  $scope.debit = []
  $scope.credit = []

  # Methods

  $scope.deleteCategory = (category)->
  	$scope.categories.delete category.id
  	$scope.getCategories()
  
  $scope.getCategories = ->
    $scope.debit = $scope.categories.debit()
    $scope.credit = $scope.categories.credit()


  # Initialization
  $scope.getCategories()

  console.log($scope.categories_data);

  return ctrl

# Information about category
.controller 'CategoryCtrl', ($scope, $stateParams, Categories)->

  ctrl = {}
  $scope.category = Categories.get($stateParams.categoryId)

  console.log($scope.category)
  console.log($stateParams.categoryId)

  return ctrl


##########


.controller 'FriendsCtrl', ($scope, Friends)->
  $scope.friends = Friends.all();
  ctrl = {}
  return ctrl

.controller 'FriendDetailCtrl', ($scope, $stateParams, Friends)->
  $scope.friend = Friends.get($stateParams.friendId);
  ctrl = {}
  return ctrl

